#!/usr/bin/python
# -*- coding: utf-8 -*-

aeras = 'CN,HK'

res = set()

def pars(line):
	sps = line.split('|')
	for aera in aeras:
		if aera in line and 'ipv4' in line :
			duan = '%s/%s' % (sps[3], (32 - (len(bin(long(sps[4])-1))-2)))
			if duan not in res:
				res.add(duan)
				print duan


for line in open('delegated-apnic-latest').readlines():
	line = line.strip()
	pars(line)


